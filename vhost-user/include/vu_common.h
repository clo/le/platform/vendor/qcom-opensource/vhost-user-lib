/* Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>

typedef uint64_t le64;
typedef uint32_t le32;
typedef uint16_t le16;


#endif
